Rails.application.routes.draw do
  #get 'pages/about'
  get '/about', to: 'pages#about'

  #get 'pages/locations'
  get '/location', to: 'pages#locations'
  #get 'pages/contact'
  get '/contact', to: 'pages#contact'
  #get 'pages/home'
  get '/home', to: 'pages#home'
  
  root 'pages#home'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
