class PagesController < ApplicationController
  def about
  end

  def locations
  end

  def contact
  end

  def home
  end
end
